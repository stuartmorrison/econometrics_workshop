# HoustonKemp econometrics workshop at USyd
### Stuart Morrison & Dylan Frangos

The *Example_solutions-HoustonKemp_Econometrics_workshop* zip file above provides data files and example code solutions for running some analysis, including:

- an Excel workbook;
- a CSV file for those use R, Stata or similar for their analysis; and
- R and Stata example solutions.
